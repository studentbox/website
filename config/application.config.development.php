<?php

return array(
    'modules' => array(
        'ZendDeveloperTools',
        'EdpSuperluminal',
    ),
    'module_listener_options' => array(
        'check_dependencies' => true,
    ),
);
